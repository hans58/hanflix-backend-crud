@extends('layoutMovie')

@section('content')
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Hanflix | Watch Movie for free | Livestream</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('movies.create') }}"> Create New Movie</a>
        </div>
    </div>


@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>Thumbnail</th>
        <th>Title</th>
        <th>Movie Url</th>
        <th>Description</th>
        <th>Language</th>
        <th>Year</th>
        <th>Created at</th>
        <th>Updated at</th>
        <th width="280px">Action</th>
    </tr>

    @foreach ($movies as $movie)
    <tr>

        <!-- <td>{{ ++$i }}</td> -->
        <td><img src="/images/thumbnails/{{ $movie->thumbnail }}" width="100px"></td>
        <td>{{ $movie->title }}</td>
        <td>{{ $movie->movie_url }}</td>
        <td>{{ $movie->description }}</td>
        <td>{{ $movie->language }}</td>
        <td>{{ $movie->year }}</td>
        <td>{{ $movie->created_at->format('m/d/Y H:i') }}</td>
        <td>{{ $movie->updated_at->format('m/d/Y H:i') }}</td>
        <td>
            <form action="{{ route('destroyMovie',$movie->id) }}" method="POST">
                <a class="btn btn-warning" href="{{ route('showMovie',$movie->id) }}">Show</a>
                <a class="btn btn-info" href="{{ route('editMovie',$movie->id) }}">Edit</a>
                @csrf

                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach

</table>
{!! $movies->links() !!}
@endsection