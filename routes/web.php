<?php

use Illuminate\Support\Facades\Route;
// 
use App\Http\Controllers\MovieController;
use App\Http\Controllers\GenreController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::resource('movies', MovieController::class);

Route::get('/',[MovieController::class, 'indexMovie'])->name('indexMovie');
Route::get('/create',[MovieController::class, 'createMovie'])->name('createMovie');
Route::post('store/',[MovieController::class, 'storeMovie'])->name('storeMovie');
Route::get('showMovie/{movie}',[MovieController::class, 'showMovie'])->name('showMovie');
Route::get('edit/{movie}',[MovieController::class, 'editMovie'])->name('editMovie');
Route::put('edit/{movie}',[MovieController::class, 'updateMovie'])->name('updateMovie');
Route::delete('/{movie}',[MovieController::class, 'destroyMovie'])->name('destroyMovie');


Route::resource('genres', GenreController::class);
Route::get('/',[GenreController::class, 'index'])->name('index');
Route::get('/create',[GenreController::class, 'create'])->name('create');
Route::post('store/',[GenreController::class, 'store'])->name('store');
Route::get('showGenre/{genre}',[GenreController::class, 'show'])->name('show');
Route::get('editGenre/{genre}',[GenreController::class, 'edit'])->name('edit');
Route::put('editGenre/{genre}',[GenreController::class, 'update'])->name('update');
Route::delete('/{genre}',[GenreController::class, 'destroyGenre'])->name('destroyGenre');

